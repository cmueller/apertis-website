+++
date = "2018-06-25"
weight = 100

title = "folks-eds-compatibility"

aliases = [
    "/qa/test_cases/folks-eds.md",
    "/old-wiki/QA/Test_Cases/folks-eds",
    "/old-wiki/QA/Test_Cases/folks-eds-compatibility"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
