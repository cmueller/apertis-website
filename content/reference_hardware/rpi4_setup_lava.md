+++
date = "2021-06-26"
weight = 100

title = "Raspberry Pi 4 Setup in LAVA"
+++

These are the instructions to prepare a device for use as a DUT in the
Apertis LAVA automated test laboratory. See
[Reference_Hardware/rpi4_setup]( {{< ref "/reference_hardware/rpi4_setup.md" >}} )
for the recommended setup for developers' devices.

# Power control

It is possible to use 5V PSU which can provide at least 2.5A to power the RPi4.
According the [HAT design guide](https://github.com/raspberrypi/hats/blob/master/designguide.md#back-powering-the-pi-via-the-gpio-header):

> it is possible to power the Pi by supplying 5V through the GPIO header
> pins 2,4 and GND. The acceptable input voltage range is 5V ±5%.

See the GPIO header pinout at [pinout.xyz](https://pinout.xyz/).

# Boot control

The Raspberry Pi 4 OTP first stage bootloader is using a second stage located in the EEPROM chip.

Depending on the [boot mode](https://www.raspberrypi.org/documentation/hardware/raspberrypi/bootmodes/bootflow_2711.md)
flashed into SPI EEPROM the second stage bootloader is able to boot the DUT
from SD-card, USB mass storage device or network.

# Flash EEPROM for network boot

Do the initial setup as described in [rpi4_setup]( {{< ref "/reference_hardware/rpi4_setup.md" >}} ).

Low level tuning of bootrom is possible with the tools from `rpi-eeprom` package.
A distribution which ships the `rpi-eeprom` package is needed, for instance NOOBS, Raspbian or Ubuntu.
Boot the RPi4 device with a selected distro and proceed in terminal.

Check the list of [available stable bootroms](https://github.com/raspberrypi/rpi-eeprom/tree/master/firmware/stable)
and export variable with the last available version, for example:
```
PI_EEPROM_VERSION=2021-04-29
```

Download the bootrom:
```
wget https://github.com/raspberrypi/rpi-eeprom/raw/master/firmware/stable/pieeprom-${PI_EEPROM_VERSION}.bin
```

Extract and save the default config:
```
rpi-eeprom-config pieeprom-${PI_EEPROM_VERSION}.bin > bootconf.txt
```

Change the bootrom configration file `bootconf.txt` with any editor according
[documentation](https://www.raspberrypi.org/documentation/hardware/raspberrypi/bcm2711_bootloader_config.md).

The configuration below is forcing the board to use network boot only and use `bcm2711-rpi-4-b`
directory on server side to search boot files:
```
[all]
BOOT_UART=1
WAKE_ON_GPIO=1
POWER_OFF_ON_HALT=0
BOOT_ORDER=0xf2
TFTP_PREFIX=1
TFTP_PREFIX_STR=bcm2711-rpi-4-b/
```

Generate the bootrom with updated config:
```
rpi-eeprom-config --out pieeprom-${PI_EEPROM_VERSION}-my.bin --config bootconf.txt pieeprom-${PI_EEPROM_VERSION}.bin
```

As `root` user install the bootrom for flashing the device
on next boot:
```
sudo rpi-eeprom-update -d -f ./pieeprom-${PI_EEPROM_VERSION}-my.bin
```

The above command will put the files below under `/boot`:
- `recovery.bin` -- second stage bootloader capable to flash the FW
- `pieeprom.upd` and `pieeprom.sig` -- prepared FW second stage bootloader to be
  flashed into EEPROM and its `sha256` checksum

After reboot the new bootrom will be flashed.

# Second stage bootloader

After setting the configuration as described above, the common directory `bcm2711-rpi-4-b` will
be used on TFTP server to search boot files for all RPi 4 devices.

The boot directory `bcm2711-rpi-4-b/` must contain files used for the second stage
bootloader (see the 
[contents of /boot](https://gitlab.apertis.org/pkg/raspi-firmware/-/tree/apertis/v2022dev3/boot)
from package [raspi-firmware](https://gitlab.apertis.org/pkg/raspi-firmware)).

In addition, the DTB file `bcm2711-rpi-4-b.dtb` from the kernel
used in Apertis and the `config.txt` boot configuration needs to
be copied under the same `bcm2711-rpi-4-b/` directory.
The description of variables for `config.txt` is [available online](https://www.raspberrypi.org/documentation/configuration/config-txt/README.md).
For Apertis we use singe bootable U-Boot image for RPi3 and RPi4, 64b OS and
upstream kernel, hence a minimal `config.txt` file should look like this:

```
kernel=u-boot.bin

# Switch the CPU from ARMv7 into ARMv8 (aarch64) mode
arm_64bit=1

# Use dtb with upstream names
upstream_kernel=1

# Enable serial console
enable_uart=1

# Forced core frequency to allow correct work of UART both
# for RPi 3 and 4 with DTB from upstream kernel
[pi3]
core_freq=250

[pi4]
core_freq=500
core_freq_min=500
```

# U-Boot binary

We are using U-Boot binary from the package [u-boot-rpi](https://gitlab.apertis.org/pkg/u-boot)
with minimal version `2020.04+dfsg-2co3` which has all features required
for Apertis. It is possible to use single unified binary `/usr/lib/u-boot/rpi_arm64/u-boot.bin`
suitable for both RPi3 and RPi4 or use a dedicated U-Boot for each
board version (need to set the `kernel` variable separately per board in `config.txt` file).

The `u-boot.bin` should be placed alongside the other boot files, so the minimal files subset
in the boot directory should contain the following files:
```
bcm2711-rpi-4-b.dtb
bootcode.bin
config.txt
fixup4.dat
start4.elf
u-boot.bin
```

# OS Boot

With the setup above the U-Boot is responsible for loading kernel and initramfs
according the configuration provided by `extlinux/extlinux.conf` or
boot commands for U-Boot set in LAVA job.

For health check the same [network boot configuration](https://gitlab.apertis.org/tests/apertis-test-cases/-/blob/apertis/v2022dev3/lava/profiles.yaml#L165-170)
may be used as we use for Renesas boards with minimal kernel v5.10.x.
